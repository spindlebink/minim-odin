# Mini Odin libraries

Anything small and reusable for Odin goes here. They're extracted from my in-progress game framework, [Pink](https://codeberg.org/spindlebink/pink).

* `rect_atlas` -- fast, simple rectangle packing algorithm modified to be nonrecursive from [this description](https://blackpawn.com/texts/lightmaps/) and deriving as well from the readme description in [this C++ implementation](https://github.com/TeamHypersomnia/rectpack2D)
* `clock` -- ultra simple runtime clock for keeping track of frame delta and fixed updates

## License

BSD 3-clause.
